<?php

require_once '../vendor/common.php';

const BASE_URL = 'http://localhost:8080';

class Ex1Tests extends WebTestCaseExtended {

    function indexToD() {
        $this->fetch('/');

        $this->ensureRelativeLink("d.html");
        $this->clickLink("d.html");
        $this->assertCurrentUrlEndsWith("d.html");
    }

    function dToC() {
        $this->fetch('/a/b/c/d/d.html');

        $this->ensureRelativeLink("c.html");
        $this->clickLink("c.html");
        $this->assertCurrentUrlEndsWith("c.html");
    }

    function cToE() {
        $this->fetch('/a/b/c/c.html');

        $this->ensureRelativeLink("e.html");
        $this->clickLink("e.html");
        $this->assertCurrentUrlEndsWith("e.html");
    }

    function eToB() {
        $this->fetch('/a/b/c/d/e/e.html');

        $this->ensureRelativeLink("b.html");
        $this->clickLink("b.html");
        $this->assertCurrentUrlEndsWith("b.html");
    }

    function bToIndex() {
        $this->fetch('/a/b/b.html');

        $this->ensureRelativeLink("index.html");
        $this->clickLink("index.html");
        $this->assertCurrentUrlEndsWith("index.html");
    }

    private function fetch($url) {
        $this->get(BASE_URL . $url);
    }
}

(new Ex1Tests())->run(new PointsReporter(
    [1 => 1,
     2 => 2,
     3 => 3,
     4 => 4,
     5 => 6]));
