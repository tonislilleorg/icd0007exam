<?php

$list1 = [1, 2, 3, 4];
$list2 = [5, 6];

function removeElementByValue($value, $array) {
    $key = array_search($value, $array);
    unset($array[$key]);
    return array_values($array);
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>

<form>

<select name="list1" multiple>
    <?php foreach ($list1 as $number): ?>
        <option value="<?= $number ?>"><?= $number ?></option>
    <?php endforeach; ?>
</select>

<input type="submit" name="list1_to_list2" value="&gt;&gt;">
<input type="submit" name="list2_to_list1" value="&lt;&lt;">

<select name="list2" multiple>
    <option value="5">5</option>
    <option value="6">6</option>
</select>

</form>

</body>
</html>
