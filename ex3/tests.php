<?php

require_once '../vendor/common.php';

const BASE_URL = 'http://localhost:8080';

class Ex3Tests extends WebTestCaseExtended {

    function defaultPageHasCorrectFieldsAndValues() {
        $this->get(BASE_URL);

        $this->assertField('list1');
        $this->assertField('list2');
        $this->assertField('list1_to_list2');
        $this->assertField('list2_to_list1');

        $list1options = $this->getSelectOptionValues('list1');
        $list2options = $this->getSelectOptionValues('list2');

        $this->assertEqual($list1options, [1, 2, 3, 4]);
        $this->assertEqual($list2options, [5, 6]);
    }

    function canMoveItemsFromList1ToList2() {
        $this->get(BASE_URL);

        $this->selectOption('list1', '2');
        $this->clickSubmitByName('list1_to_list2');

        $list1options = $this->getSelectOptionValues('list1');
        $list2options = $this->getSelectOptionValues('list2');

        $this->assertEqual($list1options, [1, 3, 4]);
        $this->assertEqual($list2options, [5, 6, 2]);

        $this->selectOption('list1', '1');
        $this->clickSubmitByName('list1_to_list2');

        $list1options = $this->getSelectOptionValues('list1');
        $list2options = $this->getSelectOptionValues('list2');

        $this->assertEqual($list1options, [3, 4]);
        $this->assertEqual($list2options, [5, 6, 2, 1]);
    }

    function canMoveItemsFromList2ToList1() {
        $this->get(BASE_URL);

        $this->selectOption('list2', '5');
        $this->clickSubmitByName('list2_to_list1');

        $list1options = $this->getSelectOptionValues('list1');
        $list2options = $this->getSelectOptionValues('list2');

        $this->assertEqual($list1options, [1, 2, 3, 4, 5]);
        $this->assertEqual($list2options, [6]);

        $this->selectOption('list2', '6');
        $this->clickSubmitByName('list2_to_list1');

        $list1options = $this->getSelectOptionValues('list1');
        $list2options = $this->getSelectOptionValues('list2');

        $this->assertEqual($list1options, [1, 2, 3, 4, 5, 6]);
        $this->assertEqual($list2options, []);
    }

}

(new Ex3Tests())->run(new PointsReporter(
    [1 => 0,
     2 => 6,
     3 => 10]));


